// This setup contains a working configuration for a local setup with nginx.
window.env = {
  deployBaseUrl: 'https://local.dev.pink-ribbon-belgium.org',
  deployPath: '/ui/immutable/',
  immutableBuild: '00011',
  bookmarkableBuild: '00000',
  disableAssertionChecks: true,
  enableServiceWorker: true,
  intervalVersionChecker: 600
}
