data "aws_iam_policy_document" "role-policy" {
  statement {
    effect = "Allow"
    actions = [
      "iam:Get*",
      "iam:List*",
      "iam:CreateUser",
      "iam:UpdateUser",
      "iam:DeleteUser",
      "iam:TagUser",
      "iam:UntagUser",
      "iam:AttachUserPolicy",
      "iam:DetachUserPolicy",
      "iam:CreateAccessKey",
      "iam:UpdateAccessKey",
      "iam:DeleteAccessKey"
    ]
    resources = [
      local.ui_ci-arn-user
    ]
  }
  statement {
    # devsecops users can manage devsecops policies
    effect = "Allow"
    actions = [
      "iam:Get*",
      "iam:List*",
      "iam:CreatePolicy",
      "iam:DeletePolicy",
      "iam:GetPolicyVersion",
      "iam:CreatePolicyVersion",
      "iam:DeletePolicyVersion",
      "iam:SetDefaultPolicyVersion",
    ]
    resources = [
      local.ui_ci-arn-policy
    ]
  }
}

resource "aws_iam_policy" "role-policy" {
  name        = local.infrastructure-prefix
  policy      = data.aws_iam_policy_document.role-policy.json
  path        = "/devsecops/"
  description = ""
}
