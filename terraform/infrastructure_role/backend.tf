terraform {
  backend "s3" {
    bucket         = "tfstate.pink-ribbon-belgium.org"
    key            = "pink-march-ui-bookmarkable-infrastructure_role.tfstate"
    region         = "eu-west-1"
    profile        = "pink-ribbon-belgium-dev"
    encrypt        = true
    dynamodb_table = "tfstate-lock.pink-ribbon-belgium.org"
  }
}

# Read output from immutable repo
data "terraform_remote_state" "s3_bucket" {
  backend = "s3"
  config = {
    bucket  = "tfstate.pink-ribbon-belgium.org"
    key     = "pink-march-ui-immutable-main.tfstate"
    region  = local.region
    profile = local.profile
  }
}
