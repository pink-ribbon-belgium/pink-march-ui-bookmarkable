locals {
  region                = "eu-west-1"
  profile               = "pink-ribbon-belgium-dev"
  repo-basename         = "pink-march-ui"
  repo-bookmarkable     = "${local.repo-basename}-bookmarkable"
  infrastructure-prefix = "${local.repo-bookmarkable}-infrastructure"
  ui_ci-name            = local.repo-bookmarkable
  ui_ci-path            = "/ci/"
  ui_ci-arn-user        = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user${local.ui_ci-path}${local.ui_ci-name}"
  ui_ci-arn-policy      = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.ui_ci-path}${local.ui_ci-name}"
}

data "aws_caller_identity" "current" {}
