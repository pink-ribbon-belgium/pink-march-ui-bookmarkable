output "I-infrastructure_role" {
  value = {
    path = aws_iam_role.pink-march-ui-bookmarkable-infrastructure.path
    name = aws_iam_role.pink-march-ui-bookmarkable-infrastructure.name
    arn  = aws_iam_role.pink-march-ui-bookmarkable-infrastructure.arn
  }
}

# both for ci user and policy
output "I-ui_ci" {
  value = {
    name       = local.ui_ci-name
    path       = local.ui_ci-path
    arn-user   = local.ui_ci-arn-user
    arn-policy = local.ui_ci-arn-policy
  }
}
