output "I-ui_ci" {
  value = {
    name              = aws_iam_user.ci.name
    path              = aws_iam_user.ci.path
    arn               = aws_iam_user.ci.arn
    aws_access_key_id = aws_iam_access_key.ci.id
  }
}

output "I-ui_ci-aws_secret_access_key" {
  value     = aws_iam_access_key.ci.secret
  sensitive = true
}
