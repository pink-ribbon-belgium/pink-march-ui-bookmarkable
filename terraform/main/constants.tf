locals {
  region  = "eu-west-1"
  profile = "pink-ribbon-belgium-dev"
  repo    = "pink-march-ui-bookmarkable"
  tags = {
    name = "Pink March UI bookmarks"
    env  = "production"
    repo = "${local.repo}//terraform/main"
  }

}
