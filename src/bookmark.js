'use strict'

/**
 * The problem: ngsw.jso isn't prefixed correctly and there is no angular way to configure these files.
 * Solution: This script will override the assetGroup content of ngsw.json with our 'servePath' (found in angular.json)
 * + 'filename'.
 * We use this script every time we build our angular project.
 */
const path = require('path')
const fs = require('fs')
const sha1 = require('./sha').sha1Binary
const configPath = path.join(__dirname, '..', 'config.js')
const indexHtmlPath = path.join(__dirname, '..', 'dist', 'index.html')
const ngswPathJson = path.join(__dirname, '..', 'dist', 'ngsw.json')
const ngswJson = require(ngswPathJson)
const immutableBuild = process.env.immutableBuild || '00000'

const bookmarkNgswJson = () => {
  const indexHtmlName = 'index.html'

  const indexHtml = fs.readFileSync(indexHtmlPath)
  const shaIndexHtml = sha1(indexHtml)
  const key = '/ui/immutable/' + immutableBuild + '/' + indexHtmlName
  const newKey = '/ui/bookmarkable/' + buildNumber + '/' + indexHtmlName
  ngswJson.hashTable[newKey] = shaIndexHtml
  delete ngswJson.hashTable[key]
  const rootKey = indexHtmlName
  ngswJson.hashTable[rootKey] = shaIndexHtml

  fs.writeFileSync(ngswPathJson, JSON.stringify(ngswJson, null, 4))
}

const bookmarkIndexHtml = () => {
  //       sub(/'<script src="config.js"><\/script>'/,"<script>\n\t\t'$(printf "%q" $config)'\n\t</script>");
  const config = fs.readFileSync(configPath).toString('UTF-8')
  const bookmarkedConfig = config.replace(
    /bookmarkableBuild: '00000'/g,
    "bookmarkableBuild: '" + buildNumber + "'"
  )
  const indexHtml = fs.readFileSync(indexHtmlPath).toString('UTF-8')
  const bookmarkedScoringHtml = indexHtml.replace(
    /<script src="config.js"><\/script>/g,
    '<script>\n' + bookmarkedConfig + '\t</script>'
  )

  fs.writeFileSync(indexHtmlPath, bookmarkedScoringHtml)
}

const bookmark = () => {
  bookmarkIndexHtml()
  bookmarkNgswJson()
}

const buildNumber = process.env.bookmarkable_build || '00000'
bookmark()
