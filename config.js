window.env = {
  deployBaseUrl: 'https://app.pink-march.pink-ribbon-belgium.org',
  deployPath: '/ui/immutable/',
  immutableBuild: '01836',
  bookmarkableBuild: '00000',
  disableAssertionChecks: true,
  enableServiceWorker: true,
  intervalVersionChecker: 600,
  apiStage: 'api-01469'
}
