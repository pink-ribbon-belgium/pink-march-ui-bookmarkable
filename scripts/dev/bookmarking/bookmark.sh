#!/usr/bin/env bash

set -x
#    Copyright 2018-2018 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is  distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

immutable_build_index_name="index.html"
immutable_build_ngsw_name="ngsw.json"
immutable_build_ngswworker_name="ngsw-worker.js"
if [[ $CI && ${CI+X} ]]
then
  immutable_build_base_url="https://app.pink-march.pink-ribbon-belgium.org/ui/immutable/"
  bookmarkable_build_base_url="https://app.pink-march.pink-ribbon-belgium.org/ui/bookmarkable/"
  immutable_build=`cat config.js | grep "immutableBuild" | awk -F "'" '{print $2}'`
else
  immutable_build_base_url="https://local.dev.pink-ribbon-belgium.org/ui/immutable/"
  bookmarkable_build_base_url="https://local.dev.pink-ribbon-belgium.org/ui/bookmarkable/"
  immutable_build="00000"
fi
export bookmarkable_build=`printf %05d ${BITBUCKET_BUILD_NUMBER}`
immutable_build_url="$immutable_build_base_url$immutable_build/"

mkdir -p ./dist

for asset in "index.html" "ngsw.json" "ngsw-worker.js" "fitbit.html" "googlefit.html" "polar.html"
do
  wget -qO- $immutable_build_url$asset > ./dist/$asset
done

npm run bookmark
